(function($) {


    var Drum = function(el, options) {
        options.modulid = "#" + $(el).attr("id");
        // 当前active number
        var cur = 0, show;
        var n = $(options.modulid + " ul li").length - 1;
        if(n == -1) {
            console.warn('app数据为空！');
            return ;
        } else if(n == 1) {
            // show = [1, 0, 1];
            // $(options.modulid + " ul li").eq(1).clone().appendTo($(options.modulid + " ul"))
        }
        
        show = this.show = [n, cur, cur + 1];

        this.cur = cur;

        this.n = n;

        this.timer = null;
        
        var s = this.modulid = options.modulid;

        this.start(options);
    }

    Drum.prototype = {

        start: function(options) {
            var cur = this.cur,
                show = this.show,
                n = this.n;

            function addClass(type) {
                
                if(n == 1) {
                    $(options.modulid + " ul li").eq(show[0]).addClass("fnt-bottom2");
                    $(options.modulid + " ul li").eq(show[1]).addClass("fnt-active2");
                }else {
                    $(options.modulid + " ul li").eq(show[0]).addClass("fnt-top");
                    $(options.modulid + " ul li").eq(show[1]).addClass("fnt-active");
                    $(options.modulid + " ul li").eq(show[2]).addClass("fnt-bottom");
                }
                type === 'init'
                    ? typeof options.init === "function" && options.init( $(options.modulid + " ul li").eq(show[1]).data() )
                    : typeof options.change === "function" && options.change( $(options.modulid + " ul li").eq(show[1]).data() );

            }

            function decrease() {
                cur--;
                if (cur < 0) cur = n;
                changeClass()
            }

            function increase() {
                cur++;
                if (cur == n + 1) cur = 0;
                changeClass()
            }

            function changeClass() {
                $(options.modulid + " ul li").attr("class", "");

                if (cur == 0) {
                    show[0] = n;
                    show[1] = cur;
                    show[2] = cur + 1;                        
                } else if (cur == n) {
                    show[0] = n - 1;
                    show[1] = cur;
                    show[2] = 0;
                } else {
                    show[0] = cur - 1;
                    show[1] = cur;
                    show[2] = cur + 1;
                }

                addClass()
            }
            
            // $(options.modulid + " ul li").click(function(e) {
            //     cur = $(this).index();
            //     changeClass()
            // });
            
            $(options.modulid).css({
                padding: "0px 10px",
                width: options.width
            })

            addClass('init');
            $(options.modulid + ">div").click(function(e) {
                if ($(this).attr("class") == "fnt-top-arrow") decrease();
                else increase()
            });
            if (options.autoplay) {
                this.timer = setInterval(function() {
                    increase()
                }, options.timer);
                
            }
        },

        destroy: function() {
            clearInterval(this.timer);
            $(this.modulid).empty();
        }
    }

    Drum.DEFAULTS = {
        width: "100%",
        modulid: "#funnyNewsTicker",
        autoplay: true,
        timer: 3e3,
        titlecolor: "#333",
        titlefontsize: "16px",
        itembgcolor: "#FFF",
        contentlinkcolor: "#099",
        infobgcolor: "#f2f2f2",
        bordercolor: "#DDD",
        itemheight: 130,
        infobarvisible: true,
        btnlinkbutton: true,
        btnlinktarget: "_blank",
        pagecountvisible: true,
        buttonstyle: "black",
        change: function() {},
        init: function() {},
        
    };


    $.fn.funnyNewsTicker = function(opts) {
       
        var value,
            args = Array.prototype.slice.call(arguments, 1);

        this.each(function() {

            var $this = $(this),
                data = $this.data('br.drum'),
                options = $.extend({}, Drum.DEFAULTS, $this.data(),
                    typeof opts === 'object' && opts);

            if (typeof opts === 'string') {

                if (!data) {
                    return;
                }

                value = typeof data[opts] === "function" && data[opts].apply(data, args);

                if (opts === 'destroy') {
                    $this.removeData('br.drum');
                }
            }

            if (!data) {
                $this.data('br.drum', (data = new Drum(this, options)));
            }

        });

        return typeof value == 'undefined' ? this : value;
    }
    
})(jQuery);